package com.cms.entity;

import java.util.List;

import com.cms.entity.base.BaseForm;
import com.cms.util.DbUtils;
import com.jfinal.plugin.activerecord.Page;

/**
 * Entity - 表单
 * 
 * 
 * 
 */
@SuppressWarnings("serial")
public class Form extends BaseForm<Form> {
	
    /**
     * 查找所有表单
     * 
     */
    public List<Form> findAll(){
        return find("select * from cms_form");
    }
	
	/**
	 * 查找表单分页
	 * 
	 * @param pageNumber
	 *            页码
	 * @param pageSize
	 *            每页记录数
	 * @return 表单分页
	 */
	public Page<Form> findPage(Integer pageNumber,Integer pageSize){
	    String filterSql = "";
		String orderBySql = DbUtils.getOrderBySql("createDate desc");
		return paginate(pageNumber, pageSize, "select *", "from cms_form where 1=1 "+filterSql+orderBySql);
	}
}
